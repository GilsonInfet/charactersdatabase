import axios from 'axios'
import { getToken } from '../config/auth'
import history from './history'


/**
 * Instância do axios configurada para rota base localhost 3001
 */
const clientHttp = axios.create({
    // baseURL: `http://localhost:3001`,
    baseURL: `https://charactersdatabase.herokuapp.com/`
    // timeout: 10000
})

clientHttp.defaults.headers['Content-Type'] = 'application/json';
clientHttp.defaults.headers['x-forwarded-proto'] = 'https';

if (getToken()) {
    clientHttp.defaults.headers['x-auth-token'] = getToken();
}


const interceptor = clientHttp.interceptors.response.use(
    response => response,
    error => {
        // Error
        const { response: { status } } = error;

        console.log ( error)

        if (error.message === 'Network Error' && !error.response) {
            alert('você está sem internet')
        }

        switch (status) {
            case 401:
                console.log('401', 'sem autorização')
                // removeToken()
                history.push('/login')
                break;
            case 403:
                history.push(`/erro/403`)
                break;
            default:
                console.log(status, `Aconteceu um erro ${status}`)

                history.push(`/erro/${status}`)

                break;
        }
        axios.interceptors.response.eject(interceptor);
        return Promise.reject(error);
    }
);



export {
    clientHttp
}
