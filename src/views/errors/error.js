import React from 'react'
import { useParams } from 'react-router-dom'


const messages = {
    '400' : "Erro Bad Request",
    '403': "Você não tem autorizacão",
    '404': "A requisição retornou vazio",
    '500': "Ocorreu um erro, tente novamente"
}


const Error = () => {
    const { erro } = useParams()
    console.log("Use Param erro: ", erro)

    return (
        <h1>
            [{erro || 500}] -  {messages[erro] || messages['500']}
        </h1>
    )
}

export default Error
