import React, { useEffect, useState } from 'react'
import Layout from '../components/layout/layout'
import UserList from '../components/user/list'
import UserCreate from '../components/user/create'
import Testes from '../components/personagens/testes'
import jwt from 'jsonwebtoken'

// //todo
import PersonagemList from '../components/personagens/listPersonagem'
import PersonagensCreate from '../components/personagens/createPersonagem'
import { getToken } from '../config/auth';

import { Route, Switch, Redirect } from "react-router-dom";

const User = (props) => {
    const [useinfo, setuseinfo] = useState({})
    const [userIsAdmin, setUserIsAdmin] = useState({})

    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setuseinfo(user)
            setUserIsAdmin(user.is_admin)
        })()

        return () => { }
    }, [])



    const AdminRoute = ({ ...rest }) => {
        console.log("AdminRoute ...rest: ", { ...rest })
        if (rest.admin) {
            if (!userIsAdmin) {
                return <Redirect to="/403" />
            }
            return <Route {...rest} basename={props.match.path} />
        }
        return <Route {...rest} basename={props.match.path} />
    }




    return (
        <Layout info={useinfo} showNav={true}>

            <Switch>
                {/* <Route  exact path={props.match.path} component={Testes} /> */}
                <Route  exact path={props.match.path + "list"} component={UserList} />
                <AdminRoute  admin= {"ddddd"} exact path={props.match.path + "create"} component={UserCreate} />
                <AdminRoute  admin exact path={props.match.path + "edit/:id"} component={UserCreate} />
                <Route  admin exact path={props.match.path + "personagemList"} component={PersonagemList} />
                <AdminRoute  admin exact path={props.match.path + "personagensCreate"} component={PersonagensCreate} />
                <Route exact path={"/403"} component={() => <h1>Você não tem Permissão. Apenas admin tem acesso para create e edit</h1>} />
            </Switch>
        </Layout>
    )
}

export default User





