import React from 'react'
import Login from './components/login/login'
import User from './views/User';
import { isAuthenticated } from './config/auth';

import ErrorHandler from './views/errors/error';
import history from '../src/config/history'
import {  Switch, Route, Redirect, Router } from "react-router-dom";



const CustomRoute = ({ ...rest }) => {

    console.log("CustomRoute ...rest" , { ...rest })
    if (!isAuthenticated()) {
          alert('vc não está auteticado')
        return <Redirect to='/login' />

    }
    return <Route  {...rest} />
}




const Routers = (props) => {

    return (
        <Router history={history} >
            
            <Switch>
            
                <Route exact path="/login" component={Login} />
                <Route exact path="/erro/:erro" component={ErrorHandler} />
                {/* <Route exact path="/qqc">  <p>TESTE Em routesjs</p> </Route> */}
                <CustomRoute path="/" component={User} /> 

            </Switch>

        </Router>
    )
}

export default Routers
