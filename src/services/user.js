import { clientHttp } from '../config/config.js'

//originais da thais
// const createUser = (data) => clientHttp.post(`/api/user`, data)
// const ListUser = () => clientHttp.get(`/api/user`)


//para rodar com meu servidro local Descomentar
const createUser = (data) => clientHttp.post(`/user`, data)
const ListUser = () => clientHttp.get(`/user`)
const deleteUserById = (data) => clientHttp.delete(`/user/${data}`)
const showUserId = (id) => clientHttp.patch(`/user/${id}`)
const updateUser = (data) => clientHttp.put(`/user/${data._id}`, data)


/**Irá retornar o token dado pelo sewrver se o 
 * login for sucesso
 */
const userAuth = async (data) => {
  return  await clientHttp.post(`/auth`, data)
}


/**
 * retorna dados do user com o email informado
 * @param {email_de_login} data 
 */
const getUserById = (data) => clientHttp.get(`/user/${data}`)

const createPersonagemNovo = (data) => clientHttp.post(`/personagem`, data) //.catch((error)=> console.log(";;;", error.toJSON()))

const axiosEditCurrentUser = (id, data) => clientHttp.patch(`/user/${id} `, data) 

/**
 * Manda requisição para o servidor com comando patch
 * @param {id único do char no mongodb} id 
 * @param {*dados a serem patcheados} data 
 */
const axiosEditCurrentPersonagem = (id, data) => {
 console.log("axiosEditCurrentPersonagem", id)
 console.log("axiosEditCurrentPersonagem", data)
  clientHttp.patch(`/personagem/${id}`, data) 
}
const ListPersonagens = () => clientHttp.get(`/personagem`)

const deletePersonagem = (data) => clientHttp.delete(`/personagem/${data}`)

/**Passar Id para mostrar profile de personagem */
const mostrarProfilePersonagem = (data) => clientHttp.get(`/poderes/${data}`)



export {
    axiosEditCurrentUser,
    createUser,
    ListUser,
    createPersonagemNovo,
    ListPersonagens,
    deletePersonagem,
    axiosEditCurrentPersonagem,
    getUserById,
    userAuth,
    deleteUserById,
    showUserId,
    updateUser,
    mostrarProfilePersonagem
}

