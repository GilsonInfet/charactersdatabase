//usa https://fontawesome.com/v4.7.0/examples/

import React, { useState } from 'react';
import { userAuth } from '../../services/user'
import { useHistory } from 'react-router-dom';
import { saveToken } from '../../config/auth'
import { clientHttp } from '../../config/config'
import './login.css'
import Layout from '../layout/layout'
import Alert from '../alert'

const Login = (props) => {


    const [auth, setAuth] = useState({})
    const [useinfo, setuseinfo] = useState({})
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState("")
    const history = useHistory()


    const handleChange = (event) => {

        setAuth({
            ...auth,
            [event.target.name]: event.target.value
        })
        return;
    }


    //descomentar aqui na versão final
    const isValidSubmit = () => auth.email && auth.senha

    /**Submete para login em evento click enter */
    const pressEnter = (event) => event.key === 'Enter' ? SubmitLogin() : null

    /**envia submit para login
     * 
     */
    const SubmitLogin = async () => {
        
        if (isValidSubmit()) {
            setLoading(true)
            try {

                const { data: { token } } = await userAuth(auth)
                saveToken(token)
                clientHttp.defaults.headers['x-auth-token'] = token;
                history.push('/list')


            } catch (error) {
                setLoading(false)
                console.log(error.response)
                if (error.response) {
                    const erroCurrent = error.response.data.errors
                    const allItens = erroCurrent.map(item => item.msg)
                    const allItensToString = allItens.join('-')
                    setError(allItensToString)
                } else {
                    setError("Não houve resposta do servidor.")
                }
            }
        }
        return;
    }




    return (

        <div>
            <Layout info={useinfo}>

                    <div className="form_login">
                        <div>
                            <label htmlFor="auth_login">Login</label>
                            <input onKeyPress={pressEnter} disabled = {loading} type="email" id="email" name="email" onChange={handleChange} value={auth.email || ""} placeholder="Insira seu e-mail" />
                        </div>
                        <div>
                            <label htmlFor="auth_password">Senha</label>
                            <input onKeyPress={pressEnter} disabled = {loading}  type="password" id="senha" name="senha" onChange={handleChange} value={auth.senha || ""} placeholder="Insira sua senha" />
                        </div>


                        <button disabled={!isValidSubmit()} onClick={SubmitLogin} >
                            {loading ? (<i className="fa fa-spinner fa-spin fa-3x fa-fw">   </i>) : "Entrar"}
                        </button> 
                     
                      
                        <div className="alertLogin">
                            <Alert show={error ? true : false} type="error" message={error} />
                        </div>
                    </div>
                
            </Layout>
        </div>

    )
}


export default Login;
