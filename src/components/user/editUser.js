import React, { useState, useEffect  } from 'react'
import { axiosEditCurrentUser } from '../../services/user'


/**
 * Componente para edição do usuário. Baseado no form de cadastro.
 * @param {userEmEdição} props 
 */
const EditUser = (props) => {

    const [userData, setuserData] = useState({
        registradoPor: props.registradoPor
    })


    const saveuserData = (event) => {
        setuserData({
            ...userData,
            [event.target.name]: event.target.value,
            
        })
        return;
    }


    const CancelEditing = () => {
        console.log("Cancell edit user")
        props.setUserEditing({_id:""})
    }

    const editCurrentUsuario = async (event) => {

        try {

            if (props.registradoPor === "") {
                alert("Função permitida apenas para users logados")
            } else {
                console.log(props.userEditing)
                 await axiosEditCurrentUser(props.userEditing._id, userData)
                alert(`Usuário  ${props.userEditing.nome} editado`)

                props.setUserEditing({_id:""})
                reLoad()
            }
        } catch (error) {
            alert('Erro no envio de editar Personagem ' + error)
        }
    }


    /**
    * Executa uma atualização da tela de listar personagem.
    */
    const reLoad = () => {
        props.refreshList();
    }



 

    useEffect(() => {  
        // fillFormEdit()
       
    })



    return (

        <div className="form_login">

            <div className="form-group"> EDITANDO : {props.userEditing.nome}  <p> &nbsp; </p>   </div>
            <hr></hr> <p>&nbsp;  </p>

            <div className="form-group">
                <label forname="inputdefault" >Nome do Usuário:</label>
                <input onChange={saveuserData} className="form-control" id="nome" name="nome" type="text" defaultValue={props.userEditing.nome} />
            </div>

            <div className="form-group">
                <label forname="vida">Email:</label>
                <input onChange={saveuserData} className="form-control input-lg" id="email" name="email" type="email" defaultValue={props.userEditing.email}  />
            </div>

            <div className="form-group">
                <label forname="inputsm">Senha</label>
                <input onChange={saveuserData} className="form-control input-sm" id="ataque" name="ataque" type="password" defaultValue={props.userEditing.senha} />
            </div>



            <div className="btns">

                <button disabled={false} onClick={editCurrentUsuario} className="btn">Confirmar</button>
                <button disabled={false} onClick={CancelEditing} className="btn">Cancelar</button>
            
            
            </div>
        </div>
    )

}

export {
    EditUser
}