import React, { useState, useRef, useEffect } from 'react'
import { createUser, showUserId, updateUser } from '../../services/user'
import { useHistory, useParams , Link } from 'react-router-dom'
import Alert from '../alert/index'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'
// import user from '../../components/layout/nav/nav'
import './user.css'

const UserCreate = (props) => {

    const inputName = useRef(null);
    const inputEmail = useRef(null);
    const inputPassword = useRef(null);
    const [isSubmit, setIsSubmit] = useState(false)
    const { id } = useParams()
    const [alert, setAlert] = useState({})
    const history = useHistory()
    const [isEdit, setisEdit] = useState(false)
    const [userIsAdmin, setUserIsAdmin] = useState({})
    const methodUser = isEdit ? updateUser : createUser

    const [form, setForm] = useState({
        // is_admin: false
    })



    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setUserIsAdmin(user.is_admin)
        })()
        return () => { }
    }, [])

    useEffect(() => {
        const getShowUser = async () => {
            const user = await showUserId(id)
            if (user.data.senha) {
                // delete user.data.senha
                alert("tem senha")
            }
            setForm(user.data)
        }


        if (id) {
            setisEdit(true)
            getShowUser()
        }

    }, [id])



    const handleChange = (event) => {

         const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
         const name = event.target.name;

            setForm({
                ...form,
                [name]: value
            });
        
    }

    const formIsValid = () => {
        return form.nome && form.email && form.senha
    }

    const submitForm = async (event) => {
        try {
            setIsSubmit(true)

            await methodUser(form)
            //await createUser(form)

            setForm({
                ...form,
                is_admin: false,
            })


            setAlert({
                type: "success",
                message: 'Seu formulário foi enviado com sucesso',
                show: true
            })

            setIsSubmit(false)

            setTimeout(function () { history.push("/list") }, 3000)

        } catch (e) {
            setAlert({
                type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: true
            })
            setIsSubmit(false)
        }

    }

    const cancelEdit = () =>{
        setIsSubmit(false)
         history.push("/list") 

    }

   


    return (
     

            

                <div className="form_login">
                    <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />


                    <div>
                        <label htmlFor="auth_nome">Nome:</label>
                        <input ref={inputName} type="text" id="auth_nome" name="nome" onChange={handleChange} value={form.nome || ""} placeholder="Insira o nome do usuário" />
                    </div>
                    <div>
                        <label htmlFor="auth_email">Email:</label>
                        <input ref={inputEmail} type="email" id="auth_email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Insira email do usuário" />
                    </div>
                    <div>
                        <label htmlFor="auth_password">Senha:</label>
                        <input ref={inputPassword} type="password" id="auth_password" name="senha" onChange={handleChange} value={form.senha || ""} placeholder="Insira senha para o usuário" />
                    </div>

                    {userIsAdmin ? (
                        <div>
                            <label htmlFor="is_admin">Ativar Usuário como administrador:</label>
                            {/* <input type="checkbox" name="is_admin" id="is_admin" onChange={handleChange} checked={(!isEdit ? true : form.is_admin) || false} /> */}
                            <input type="checkbox" name="is_admin" id="is_admin" onChange={handleChange} checked={form.is_admin || false} />
                        </div>
                    )
                        : ""}


                    <div>
                        <label htmlFor="is_active" className={!isEdit ? 'colorgray' : ''}>Usuário ativo?</label>
                        <input type="checkbox" name="is_active" onChange={handleChange} id="is_active" checked={form.is_active || false} />
                    </div>


                    <button className="createButtons" disabled={!formIsValid()} onClick={submitForm}>{(isEdit ? 'Alterar' : 'Cadastrar')} </button>
                    <button className="createButtons" onClick={cancelEdit}>Cancelar</button>
                </div>
            
     
    )
}

export default UserCreate
