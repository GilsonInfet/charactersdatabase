import React, { useEffect, useState } from 'react'
import { ListUser, deleteUserById  } from '../../services/user'
import Loading from '../loading/loading'
import './user.css'
// import {EditUser} from '../user/editUser'

const UserList = (props) => {
    const [users, setUsers] = useState([]) //descomentar
    // const [isEditing, setisEditing] = useState("")
    const [loading, setloading] = useState(false)
    const [userEditing, setUserEditing] = useState({
        _id: ""
    })


    const [confirmation, setConfirmation] = useState({
        isShow: false,
        params: {}
    })


    const editUser = (user) => props.history.push(`/edit/${user._id}`)

    const getList = async () => {
        try {
            setloading(true)

            const usersTodos = await ListUser()

            if (usersTodos){ 
             setUsers(sortList(usersTodos.data))
            }
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }


    /**Chama setUserEditing que seta userEditing e exibe o form de edit user */
    // const editUser = (user) => {
        
    //     setUserEditing(user) 
    //     console.log("Editando", user.nome)      
    // }



    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.is_active < b.is_active) {
                return 1;
            }
            if (a.is_active > b.is_active) {
                return -1;
            }
            return 0;
        })
    }



    


    const deleteUser = async (user) => {

        var answer = window.confirm("Confirma exclusão de " + user.email + " ?");
        if (answer) {
        
            try {                
                await deleteUserById(user._id).catch(err => {
                    console.log(err);
                });

                setUsers([])
                setTimeout(getList(), 3000)

            } catch (error) {
                console.log('não fez delete user ', error)
            }
        }
    }


    const estilo = {cursor: 'pointer' , 
                   textAlign: 'center' 
                   }

    const montarUsuarios = () => users.map((user, index) => (

        
        <tr key={index} className={user.is_active ? "" : "noActive"}>
            <td>{user.nome}</td>
            <td>{user.email}</td>
            <td>{user._id}</td>

            <td>{user.is_active ? "SIM" : "NÃO"}</td>
            <td>{user.is_admin ? "SIM" : "NÃO"}</td>

            <td style={estilo} >
                <span onClick={() => editUser(user)} > <i className="fa fa-pencil-square-o" aria-hidden="true"></i>  Editar</span> |
                <span onClick={() => deleteUser(user)}> <i className="fa fa-trash" aria-hidden="true"> </i>  Excluir </span>
            </td>
        </tr>
    ))

    useEffect(() => {
        getList()       

    }, [])

     /**
     * atualiza a lista de Pessoas após a atualização
     */
    function refreshList() {
        setUsers([])
        getList()
    }
    const verifyIsEmpty = users.length === 0


    //**retorna cabeçalho e a lista de user  */
    const cabecalhoLista = () => (

        <>

           {/* { userEditing._id !== "" ? <EditUser  userEditing={userEditing} setUserEditing = {setUserEditing}  refreshList={refreshList}/> : ""}  */}

            <div className="list_user">
            {/* <Nav name="Novo" to="/create" /> */}
                <table border="1">
                    <thead>
                        <tr>
                            <th>NAME</th>
                            <th>EMAIL</th>
                            <th>ID</th>
                            <th>ATIVO</th>
                            <th>ADMINISTRADOR</th>
                            <th>AÇÕES</th>
                        </tr>
                    </thead>
                    <tbody>
                        {montarUsuarios()}
                    </tbody>
                </table>
            </div>
            {/* <button onClick={() => getList()}>USER LIST</button> */}
        </>
    )


    return (
        <>
            {!verifyIsEmpty ? cabecalhoLista() : <Loading show={true} />}

        </>
    )

}




export default UserList
