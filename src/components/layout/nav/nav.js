import React, { useState } from 'react'
// import { useHistory } from 'react-router-dom';


import history from '../../../config/history'

///// NAV COMEÇA AQUI ================================================
const Nav = (props) => {
    const [navLabel, setNavLabel] = useState('Lista de Usuários')

    const ohc = (event) => {

        history.push(event.target.value)


        switch (event.target.value) {
            case "/":
                setNavLabel("Welcome to the Characters Database")
                break;
            case "/create":
                setNavLabel("Criar Usuário")
                break;

            case "/list":
                setNavLabel("Lista de Usuários")
                break;

            case "/personagemList":
                setNavLabel("Lista de Personagens")
                break;
            case "/personagensCreate":
                setNavLabel("Criar Personagens")
                break;

            default:
                setNavLabel("")
        }

    }

    return (
        <nav className="">
            <div className="title">{navLabel} </div>

            <div > Escolha o destino &nbsp; </div>
            <select onChange={ohc} name="selecDestino" id="select">
                {/* <option value="/"></option> */}

                <option value="/list">Listar Usuários </option>
                <option value="/create">Criar Usuários</option>
                <option value="/personagemList">Listar Personagens</option>
                <option value="/personagensCreate">Criar Personagens</option>
            </select>


        </nav>
    )
}
export default Nav;
