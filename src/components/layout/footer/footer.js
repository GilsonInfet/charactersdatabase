import React from 'react'
import imgLogo from '../../../assets/img/logorbg.png'

const Footer = () => (
    <footer>
        <div className="projectName">
            CharactersDatabase
      </div>
        <div className="copyRight">
            <img src={imgLogo} alt="" />
        </div>
    </footer>
)


export default Footer
