import React  from 'react';
import Nav from '../../components/layout/nav/nav'
import Header from './header/header'
import Footer from './footer/footer'

const  Layout = (props) => {
     
    // console.log("LAYOUT PROPS: ", props)
    return (
        <div>
            <Header {...props} title="Characters Database" />



            {props.showNav === true ?   <Nav {...props} name="Nova Página Inicial"/> : ""}

            <main className="centro">
                {props.children}
            </main>
            <Footer />
        </div>
    )

}


export default Layout;
