import React from 'react'
import { useHistory } from 'react-router-dom'
import './header.css'
import { removeToken } from '../../../config/auth'


const Header = (props) => {

    // console.log('props header', props)

    const history = useHistory()

    const LogoutMe = () => {
        history.push("/login")
        removeToken()
        // var answer = window.confirm("Confirma Logoff ?");
        // if (answer) {
        //     history.push("/login")
        //     removeToken()
        // }
    }


    return (
        <header>
            <div className="title">{props.title}</div>
            <div className="profile">
            {props.info.is_admin ? `admin - ` : ""}
                {props.info.nome ? `${props.info.nome} |` : ""}
                {props.info.nome ? <button className='logoutButton'onClick={LogoutMe} > <i class="fa fa-sign-out" aria-hidden="true"></i> Sair </button>: ""}
            </div>
        </header>
    )
}


export default Header
