import React, { useState, useRef } from 'react'
import axios from 'axios'
import './testes.css'
// import history from '../../../src/config/history'

import {Course, MyRadio} from '../../setupTests'




const querystring = require('querystring');


const Testeas = (props) => {

    const instance = axios.create({
        baseURL: 'http://localhost:3001/',
        timeout: 3000,
        headers: {
            'x-forwarded-proto': 'https',
            'x-auth-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWY0NjMwMTMyYjU1MGI2NDdjMTZmMTMyIiwiZW1haWwiOiJhdXRoQGF1dGguY29tIiwibm9tZSI6IkF1dGggTWFuIn0sImlhdCI6MTU5OTI5MjY0OCwiZXhwIjoxNTk5NzI0NjQ4fQ.Vk09T2DB13WqZa-tWDb03iAeVPju7jpdsfidU829cJg"
        }
    })


    const [constCh3, setconstCh3] = useState("s")
    const [constP, setconstP] = useState("")
    const [resp, setResp] = useState("")
    const [rota, setRota] = useState("")

    const getUserById = (data) => instance.get(`/user/${data}`)
    const inputEl = useRef(null);
    const inputE2 = useRef(null);

    let email = ""


    const myFunction = async () => {
        const k = await getUserById(email)
        const { data: { nome } } = k
        // setconstP(nome)
        setconstP(JSON.stringify(k))
        console.log(typeof (nome))

        console.log(k)

    }


    /**funciona para queies simples 
     * como  {"user": "5f4630132b550b647c16f132"}
     */
    const myFunctionQ = async () => {

        // var theobj = JSON.parse(constCh3);
        // var x = querystring.stringify( JSON.parse(constCh3))
        // const q = await instance.get('profile/?'+ querystring.stringify( JSON.parse(constCh3)))
        console.log(querystring.stringify(JSON.parse(constCh3)))
        const { data } = await instance.get(rota + '/?' + querystring.stringify(JSON.parse(constCh3)))

        setResp(JSON.stringify(data))

    }





    const myFunctionParam = async () => {
        sessionStorage.setItem('chave', 'valor');
        // var param = {}
        // console.log(querystring.stringify( JSON.parse(constCh3)))

        if (!rota) {
            alert("preencha a rota")
            return
        }
        const { data } = await instance.get(rota, { params: { "vida": 200 } })

        setResp(JSON.stringify(data))

    }


    //           {"user": "5f4630132b550b647c16f132"}

    const saveInput = (e) => {
        email = e.target.value
    }


    const saveInsaveQueryput = (e) => {

        setconstCh3(e.target.value)
    }

    const saverota = (e) => {

        setRota(e.target.value)
    }

    const limpar = () => {
        // setRota("")
        // setconstCh3("")
        // setResp("")
        console.log(Course)
        // history.push("create")
        // history.go()
        console.log("process.env", process.env)
    }
    
    return (
        <>

     <MyRadio valor="seila" correto="seila"></MyRadio>

            <div className="conteiner">
                <div className="form_testes">
                    <button id="btnEmail" onClick={myFunction}>Search by Id</button>

                    <br></br>

                    <div className="form-group">
                        <label forname="inputdefault" >Pesquisa por email:</label>
                        <input ref={inputEl} onChange={saveInput} className="form-control" id="email" name="email" type="text" required />
                    </div>

                    <div className="buttonContainer">
                        <button onClick={myFunctionQ}>Search by Query</button>
                        <button onClick={myFunctionParam}>Search in Code Params</button>
                    </div>


                    <div className="form-group">
                        <label forname="inputdefault" >Pesquisa em rota: </label>
                        <input onChange={saverota} className="form-control" id="rota" name="rota" type="text" required />
                    </div>

                    <div className="form-group">
                        <label forname="inputdefault" >Pesquisa query (chave e valor com aspas, ex.: "user": "5f4630132b550b647c16f132" ):</label>
                        <input ref={inputE2} onChange={saveInsaveQueryput} className="form-control" id="nome" name="nome" type="text" required />
                    </div>
                    <button onClick={limpar}>Clear Inputs</button>
                    
                    <br />
                </div>

                <div className="rightSide">
                    <p>{constP}</p>
                    <p>{resp}</p>
                </div>
            </div>
        </>
    )


}


export default Testeas

