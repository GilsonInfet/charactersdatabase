import React, { useEffect, useState } from 'react'
import { ListPersonagens, deletePersonagem, mostrarProfilePersonagem } from '../../services/user'
import {EditPersonagem} from './editPersonagem'
import Loading from '../loading/loading'
// import Nav from '../../components/layout/nav/nav'

// get our fontawesome imports
import { faAddressCard, faPenSquare, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const PersonagemList = (props) => {
    const [pers, setPers] = useState([])

    /**
     * armazena o personagem que está sendo editado no momento
     */
    const [isEditing, setisEditing] = useState({_id:""})

    const getListChar = async () => {
        try {
            const pers = await ListPersonagens()
            setPers(pers.data)
        } catch (error) {
            console.log('error em list Personagens', error)
        }
    }

    /**Seta variável que define se char está sendo editado e isso exibe o form de correção */
    const editChar = (ppchar) => {
 
        setisEditing(ppchar)

    }

    //TODO
    const DeleteChar = async (ppchar) => {

        var answer = window.confirm("Confirma exclusão de " + ppchar.nome + " ?");
        if (answer) {
            try {
                await deletePersonagem(ppchar._id)
                alert(`Personagem  ${ppchar.nome} deletado`)
                refreshList()
            } catch (error) {
                console.log('não fez delete char ', error)
            }
        }
    }


    const mostrarProfile = async (ppchar) => {
        console.log("mostrarProfile ", ppchar._id)
        try {
            const perProfile = await mostrarProfilePersonagem(ppchar._id)

            if(perProfile){
               console.log(perProfile)
            }
            //chamar componente de exibir profile
        } catch (error) {
            //exibir mensagem se snão tiver profile
        }
    }

    const estilo = {cursor: 'pointer' , 
                   textAlign: 'center' 
                   }

    const montarCharList = () => pers.map((pchar, index) => (
        <tr key={index}>
            <td>{pchar.nome}</td>
            <td>{pchar.vida}</td>
            <td>{pchar.ataque}</td>
            <td>{pchar.defesa}</td>
            <td>{pchar.sexo}</td>
            <td>{pchar.registradoPor}</td>
            <td>{pchar._id}</td>
            {/* <i class="fas fa-id-card"></i> */}
            <td style={estilo}  >
                {/* <span onClick={() => mostrarProfile(pchar)} >  <FontAwesomeIcon icon={faAddressCard} /> Profile </span>    | */}
                <span onClick={() => editChar(pchar)} > <FontAwesomeIcon icon={faPenSquare} />  Editar </span>    |   
                <span onClick={() => DeleteChar(pchar)}>  Excluir <FontAwesomeIcon icon={faTrash} /> </span>             

            </td>
        </tr>
    ))

    useEffect(() => {
        getListChar()

    }, [])

    /**
     * atualiza a lista de personagens fazendo um reload no componente
     */
    function refreshList() {
        setPers([])
        getListChar()
    }
  

    const cabecalhoLista = () => (
        <>
        {/* <Nav/> */}
            <div className="list_user">
            {isEditing._id !== "" ? <EditPersonagem isEditing={isEditing} setisEditing = {setisEditing}  refreshList={refreshList}/> : ""}            
                
                    <table border="1">
                        <thead>
                            <tr>
                                <th>NOME</th>
                                <th>VIDA</th>
                                <th>ATAQUE</th>
                                <th>DEFESA</th>
                                <th>SEXO</th>
                                <th>REGISTRADO POR</th>
                                <th>ID</th>
                                <th>AÇÕES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {montarCharList()}
                        </tbody>
                    </table>

            </div>
            {/* <button onClick={() => getList()}>USER LIST</button> */}
        </>
    )


    return (
        <>
        {/* ta dando um warn aqui n sei pq */}
            {pers.length !== 0 ? cabecalhoLista() : <Loading show={true} />}

        </>
    )


}

export default PersonagemList


