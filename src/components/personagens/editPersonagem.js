import React, { useState, useEffect } from 'react'
import { axiosEditCurrentPersonagem } from '../../services/user'


/**
 * Componente para edição do usuário. Baseado no form de cadastro.
 * @param {userEmEdição} props 
 */
const EditPersonagem = (props) => {

    const [personagemData, setPersonagemData] = useState({
        registradoPor: props.registradoPor,
        _id: props.isEditing._id
    })




    const savePersonagemData = (event) => {
        setPersonagemData({
            ...personagemData,
            [event.target.name]: event.target.value,
            sexo: document.getElementById('sexo').value
        })
        return;
    }


    const CancelEditing = () => {
        props.setisEditing({_id:""})
    }

    const editCurrentPersonagem = async (event) => {

        try {

            if (props.registradoPor === "") {
                alert("Função permitida apenas para users logados")
            } else {


                await axiosEditCurrentPersonagem(personagemData._id, personagemData)
                alert(`Personagem  ${personagemData._id} editado`)

                props.setisEditing({_id:""})
                reLoad()
            }
        } catch (error) {
            alert('Erro no envio de editar Personagem ' + error)
        }
    }


    /**
    * Executa uma atualização da tela de listar personagem.
    */
    const reLoad = () => {
        props.refreshList();
    }



 

    useEffect(() => {  
        // fillFormEdit()
       
    })



    return (

        <div className="form_login maxwidth ">

            <div className="form-group"> EDITANDO : {props.isEditing._id}  <p> &nbsp; </p>   </div>
            <hr></hr> <p>&nbsp;  </p>

            <div className="form-group">
                <label forname="inputdefault" >Nome do Personagem:</label>
                <input onChange={savePersonagemData} className="form-control" id="nome" name="nome" type="text" defaultValue={props.isEditing.nome} />
            </div>

            <div className="form-group">
                <label forname="vida">Vida:</label>
                <input onChange={savePersonagemData} className="form-control input-lg" id="vida" name="vida" type="number" defaultValue={props.isEditing.vida}  />
            </div>

            <div className="form-group">
                <label forname="inputsm">Ataque</label>
                <input onChange={savePersonagemData} className="form-control input-sm" id="ataque" name="ataque" type="number" defaultValue={props.isEditing.ataque} />
            </div>

            <div className="form-group">
                <label forname="inputsm">Defesa</label>
                <input onChange={savePersonagemData} className="form-control input-sm" id="defesa" name="defesa" type="number" defaultValue={props.isEditing.defesa} />
            </div>

            <div className="form-group">
                <label forname="sexo">Sexo &nbsp; </label>

                <select onChange={savePersonagemData} id="sexo" name="sexo"  defaultValue={props.isEditing.sexo}>
                    <option>Masculino</option>
                    <option>Feminino</option>
                </select>

            </div>

            <div className="btns">

                <button disabled={false} onClick={editCurrentPersonagem} className="btn">Confirmar</button>
                <button disabled={false} onClick={CancelEditing} className="btn">Cancelar</button>
            </div>
        </div>
    )

}

export {
    EditPersonagem
}