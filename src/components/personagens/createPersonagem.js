import React, { useState, useRef , useEffect } from 'react'
import { createPersonagemNovo } from '../../services/user'
import history from '../../config/history'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth';
import Alert from '../alert/index'
import { Modal, Button, ModalBody, ModalFooter } from 'reactstrap'

const CreatePersonagem = (props) => {
    const inputEl = useRef(null);
    const inputE2 = useRef(null);
    const inputE3 = useRef(null);
    const inputE4 = useRef(null);
    const [alert, setAlert] = useState({
        type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: false
    })

    const [personagemData, setPersonagemData] = useState({
        // registradoPor: "props.userAtual.email"
    })



    
    const savePersonagemData = (event) => {        

        setPersonagemData({
            ...personagemData,
            [event.target.name]: event.target.value,
            sexo: document.getElementById('sel1').value,
        })
        return;
    }



    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setPersonagemData({
                ...personagemData,
                registradoPor: user.email})             
        })()

            // const x = () => <Modal > </Modal>
            // x()

            return () => { }
    },[])



    const postNewPersonagem = async (event) => {
        

        try {
            const { email } = await jwt.decode(getToken())
            setPersonagemData({registradoPor: "email"})

            await createPersonagemNovo(personagemData)
            setPersonagemData({})
            AonButtonClick()

            setAlert({
                type: "success",
                message: 'Seu formulário foi enviado com sucesso',
                show: true
            })


            setTimeout(() => { history.push('/personagemList')}, 2000)




        } catch (error) {
            setAlert({
                type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: true
            })



        }

    }



    /**zera os campos do formulário */
    const AonButtonClick = () => {
        // `current` aponta para o evento de `focus` gerado pelo campo de texto
        inputEl.current.value = ""
        inputE2.current.value = ""
        inputE3.current.value = ""
        inputE4.current.value = ""
    };



    const formIsComplete = () => {
        return personagemData.nome && personagemData.vida && personagemData.ataque && personagemData.defesa && personagemData.sexo
    }

    // const onClicar = (e) => {
    //     e.target.checked ? setPersonagemData({ isBoss: false }) : setPersonagemData({ isBoss: true })
    // }

    return (

        <>
            {/* <Nav name="Novo" /> */}

            <div className="form_login">

            <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />
                <div className="form-group">
                    <label forname="inputdefault" >Nome do Personagem:</label>
                    <input ref={inputEl} onChange={savePersonagemData} className="form-control" id="nome" name="nome" type="text" required />
                </div>

                <div className="form-group">
                    <label forname="vida">Vida:</label>
                    <input ref={inputE2} onChange={savePersonagemData} className="form-control input-lg" id="vida" name="vida" type="number" required />
                </div>

                <div className="form-group">
                    <label forname="inputsm">Ataque</label>
                    <input ref={inputE3} onChange={savePersonagemData} className="form-control input-sm" id="ataque" name="ataque" type="number" required />
                </div>

                <div className="form-group">
                    <label forname="inputsm">Defesa</label>
                    <input ref={inputE4} onChange={savePersonagemData} className="form-control input-sm" id="defesa" name="defesa" type="number" required />
                </div>



                <div className="form-group">
                    <label forname="sel1">Sexo &nbsp; </label>

                    <select onChange={savePersonagemData} id="sel1" name="sexo" required >
                        <option>Masculino</option>
                        <option>Feminino</option>
                    </select>

                </div>


                <div className="btns">
                    <button disabled={!formIsComplete()} onClick={postNewPersonagem} className="btn">Criar</button>
                </div>
            </div>
        </>
    )

}

export default CreatePersonagem